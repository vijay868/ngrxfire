import * as PostActions from './post.actions';
import { Post } from './post.model';

export type Action = PostActions.All;

/// Default app state
const defaultState: Post = {
    text: 'Hello, i am the default post',
    likes: 0
};

/// Helper function to create new state object
const newState = (state: Post, newData: Post) => {
    return Object.assign({}, state, newData);
};

export function postReducer(state: Post = defaultState, action: Action) {
    console.log(action.type, state);
    switch(action.type) {
        case PostActions.EDIT_TEXT:
            return newState(state, { likes: state.likes, text: action.payload });
        case PostActions.UPVOTE:
            return newState(state, { likes: state.likes + 1, text: state.text });
        case PostActions.DOWNVOTE:
            return newState(state, { likes: state.likes - 1, text: state.text });
        case PostActions.RESET:
            return defaultState;
        default:
            return state;
    };
}