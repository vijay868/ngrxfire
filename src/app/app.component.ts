import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { Post } from './post.model';
import * as PostActions from './post.actions';

interface AppState {
  message: string;
}

interface PostAppState {
  post: Post;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ngRxFire';

  messages$: Observable<string>;

  /// post reducer
  post: Observable<Post>;
  text: string;

  constructor(private store: Store<AppState>, private postStore: Store<PostAppState>) {
    this.messages$ = store.select('message');
    this.post = postStore.select('post');
  }

  teluguMessage(): void {
    this.store.dispatch({ type: 'TELUGU' });
  }

  englishMessage(): void {
    this.store.dispatch({ type: 'ENGLISH' });
  }

  /// post reducers dispatch actions
  editText(): void {
    this.postStore.dispatch(new PostActions.EditText(this.text));
  }

  resetPost(): void {
    this.postStore.dispatch(new PostActions.Reset());
  }

  upvote(): void {
    this.postStore.dispatch(new PostActions.Upvote());
  }

  downvote(): void {
    this.postStore.dispatch(new PostActions.Downvote());
  }
}
