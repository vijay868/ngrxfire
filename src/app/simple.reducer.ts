
import { Action } from '@ngrx/store';

export function simpleReducer(state: string = 'Hello Welcome', action: Action) {
    console.log(action.type, state);
    switch (action.type) {
        case 'TELUGU':
            return state = 'suswagatham';
        case 'ENGLISH':
            return state = 'Hello Welcome';
        default:
            return state;
    }
}
